% lisheng 12/4/17

function [dist, ho]= deepQ_update_dynamics(dist,ego_action,Xb,Yb,Xrb,Yrb)
setGlobal;
% load('Cars.mat','Cars')
Cars=evalin('base', 'Cars');
dt = Params.time_step;
ho = 0;
acc = 0;

% get current observation of each car
for car_id = 1:length(Cars)
    obs_of_all_cars(car_id) = get_Observation(car_id, Cars, Params);
end

% update position: x and y
for car_id = 1:length(Cars)
    
    if car_id ~= 1 % env car action selection
        action_id = policy_0(obs_of_all_cars(car_id), Params);
        
        if action_id ~= 7
            dynamics = act2dyn(action_id, Params);
            acc = dynamics(1);
            Cars(car_id).vy = dynamics(2);
        elseif action_id == 7 % collision
            acc = - Cars(car_id).vx / dt * 0.85;
            Cars(car_id).vy = 0;
        end
    else % ego car action selection
        [dr, bs] = get_sinr(Cars, car_id,Xb,Yb,Xrb,Yrb);
        Cars(car_id).bs1 = dr(3);
        Cars(car_id).bs2 = dr(2);
        Cars(car_id).bs3 = dr(1);
        Cars(car_id).max_bs = bs(3);
        if ego_action < 7
           dynamics = act2dyn(ego_action, Params);
           acc = dynamics(1);
           Cars(car_id).vy = dynamics(2);
        elseif ego_action == 7 % collision
           acc = - Cars(car_id). vx / dt * 0.85;
           Cars(car_id).vy = 0;
        elseif ego_action == 8 % collision
            dr_max = dr_ego;
            if dr_max == dr(1)
                Cars(car_id).max_bs = bs(3);
                Cars(car_id).bs1 = dr(1);
             
            elseif dr_max == dr(2)
                Cars(car_id).max_bs = bs(2);
                Cars(car_id).bs1 = dr(2);
             
            elseif dr_max == dr(3)
                Cars(car_id).max_bs = bs(1);
                Cars(car_id).bs1 = dr(3);
             
            end
         elseif ego_action == 9
            % 1 1 0(sorted) 1 0 1 (unsorted)
            % max 2nd max data rate
            % Thz RF check number of vehiles here.
            % multiply 1 1 0  check number of
            % quota calculating data rate
            % combine action id is 9 10
         
            dr_candidate = [dr(1), dr(2),dr(3)]; % data rate
         
            %TO BE IMPLEMENTED num of connections > quota in terms of bs,
            %coefficient = 1/quota, in terms of car, the coefficient 0
         
            %%%%%
         
%             coefficient = ones(1, length(bs_candidate)) ./ min([number_of_connected(bs_candidate, Cars), quota(bs_candidate)]); % /number_of_connected(bs_candidate, Cars);%
            coefficient = 1;
            dr_withquota = dr_candidate .* coefficient;
         
            if dr_withquota(1) > dr_withquota(2)
                Cars(car_id).max_bs = bs(3);
                Cars(car_id).bs1 = dr(1);
             
            elseif dr_withquota(2) > dr_withquota(3)
                Cars(car_id).max_bs = bs(2);
                Cars(car_id).bs1 = dr(2);
            else
                Cars(car_id).max_bs = bs(1);
                Cars(car_id).bs1 = dr(3);
            end
        else

        end
      % handovers calculation
    if (strcmpi(Cars(car_id).max_bs, Cars(car_id).prev_bs) == false)
        ho = ho + 1;
        Cars(car_id).prev_bs = Cars(car_id).max_bs; % update the base station
        %disp("hand off plus")
    end
        

    end
    
    % update velocity using dynamics
    Cars(car_id).a = acc;
    Cars(car_id).vx = Cars(car_id).vx + acc * dt;
    
    % speed limit
    if Cars(car_id).vx > Params.max_speed
        Cars(car_id).a = 0; % Cars(car_id).a = (Cars(car_id).vx - Params.max_speed)/dt;
        Cars(car_id).vx = Params.max_speed;
    elseif Cars(car_id).vx < Params.min_speed
        Cars(car_id).a = 0;
        Cars(car_id).vx = Params.min_speed;
    end
   
    
    % update position using dynamics
    % x
    Cars(car_id).x = Cars(car_id).x + Cars(car_id).vx * dt;
    % y, update y and lane_id if vy ~= 0
    if Cars(car_id).vy ~= 0
        if Cars(car_id).lane_id == 1
            Cars(car_id).y = Cars(car_id).y + Cars(car_id).vy * dt;
            Cars(car_id).lane_id = 2;
        else
            Cars(car_id).y = Cars(car_id).y - Cars(car_id).vy * dt;
            Cars(car_id).lane_id = 1;
        end
    end
    
    
    % set relative x - coordinate (wrt ego car)
    Cars(car_id).x  = Cars(car_id).x - Cars(1).vx * dt;
    Cars(car_id).theta = Cars(car_id).x / Params.road_radius;
    
    % re-regulate to -pi~pi
    if Cars(car_id).theta > pi
        Cars(car_id).theta = Cars(car_id).theta - 2*pi;
    elseif Cars(car_id).theta < -pi
        Cars(car_id).theta = Cars(car_id).theta + 2*pi;
    end
    
    Cars(car_id).x = Cars(car_id).theta * Params.road_radius;
    
    % calculate the distance ego car has travelled
    if car_id == 1
        dist = dist + Cars(car_id).vx * dt;
    end
end % update position: x and y
dist=double(dist);
% save('Cars.mat','Cars')
assignin('base', 'Cars', Cars)
return


