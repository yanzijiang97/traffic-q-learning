
import pandas as pd
import matplotlib.pyplot as plt


path = r'E:\research\1103\Keras deep Q learning\e_4000_12_24_2021_14_37_36.csv'
df = pd.read_csv(path)  
if(df['score'] > -0.2).bool:
    df['score'] = df['score'] +2
    
EPISODES, col = df.shape
print(df.shape)
print(df)

bin_num = 250
x =[]
y= []
i= 'score'
for j in range(int(EPISODES/bin_num)):
        x.append(j * bin_num)
        y.append(df[j * bin_num:j * bin_num+(bin_num-1)][i].mean())
plt.title(i)
# plt.legend(i + "_mean")
plt.plot(x, y , label = "v=20")
plt.savefig(path + i  + str(bin_num) +'.png')
plt.show()


