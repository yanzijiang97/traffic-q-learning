% lisheng 12/4/17

function [Cars, dist, ego_action, ho, dr_ego, ego_v] = Q_update_dynamics(Cars, dist, Params, net, epsilon,policy1,episode,Xb,Yb,Xrb,Yrb,NRF,NTHzT,ho_prob,time)
dt = Params.time_step;
action_num = 21; %6
ho = 0;

%acc = 0;%acceleration
%ego_v = -1; %default -1

% get current observation of each car
for car_id = 1:length(Cars)
    obs_of_all_cars(car_id) = get_Observation(car_id, Cars, Params);
    %     Cars(car_id).max_bs = obs_of_all_cars(car_id).best_bs;
    %% telecommunication side
 
    %     best_bs = bs(3);
end

% update position: x and y
for car_id = 1:length(Cars)
    tele_action = 0;

    if car_id ~= 1 % env car action selection

        action_id = policy_0(obs_of_all_cars(car_id), Params);

		    if action_id <= 18 % ~=
				dynamics = act2dyn(action_id, Params);
				acc = dynamics(1);
				Cars(car_id).vy = dynamics(2);
                tele_action = dynamics(3);

			elseif action_id >18 % collision
				acc = - Cars(car_id). vx / dt * 0.85;
				Cars(car_id).vy = 0;
                tele_action = dynamics(3);
			end
			
		
    else % ego car action selection
	       ego_state = obs2state(obs_of_all_cars(car_id), Params);
        if episode<60
        ego_action=policy1(ego_state);
        else

        if rand() <= epsilon
            rand1 = randi(action_num);
         
            while (rand1 >18)
                rand1 = randi(action_num); %avoid random number to stop state
            end
         
            ego_action = rand1; % Return ego_action = 1-18 except 19-21
		else
            [~, ego_action] = max(net(stste_trf(ego_state)));
        end
        end
%         if(ego_action <=18)
            dynamics = act2dyn(ego_action, Params);
            acc = dynamics(1);
            Cars(car_id).vy = dynamics(2);
            tele_action = dynamics(3);
            %%% access second q table choose second q table.
            
%         end
    end
    
	  % update velocity using dynamics
    Cars(car_id).a = acc;
    Cars(car_id).vx = Cars(car_id).vx + acc * dt;
 
    % speed limit
    if Cars(car_id).vx > Params.max_speed
        Cars(car_id).a = 0; % Cars(car_id).a = (Cars(car_id).vx - Params.max_speed)/dt;
        Cars(car_id).vx = Params.max_speed;
    elseif Cars(car_id).vx < Params.min_speed
        Cars(car_id).a = 0;
        Cars(car_id).vx = Params.min_speed;
    end
 
    % update position using dynamics
    % x
    Cars(car_id).x = Cars(car_id).x + Cars(car_id).vx * dt;
    % y, update y and lane_id if vy ~= 0
    if Cars(car_id).vy ~= 0
        if Cars(car_id).lane_id == 1
            Cars(car_id).y = Cars(car_id).y + Cars(car_id).vy * dt;
            Cars(car_id).lane_id = 2;
        else
            Cars(car_id).y = Cars(car_id).y - Cars(car_id).vy * dt;
            Cars(car_id).lane_id = 1;
        end
    end
 
    % set relative x - coordinate (wrt ego car)
    Cars(car_id).x = Cars(car_id).x - Cars(1).vx * dt;
    Cars(car_id).theta = Cars(car_id).x / Params.road_radius;
 
    % re-regulate to -pi~pi
    if Cars(car_id).theta > pi
        Cars(car_id).theta = Cars(car_id).theta - 2 * pi;
    elseif Cars(car_id).theta < - pi
        Cars(car_id).theta = Cars(car_id).theta + 2 * pi;
    end
 
    Cars(car_id).x = Cars(car_id).theta * Params.road_radius;
 
    % calculate the distance ego car has travelled
    %     ego_v = -1;
	
    if car_id == 1
        dist = dist + Cars(car_id).vx * dt;
        ego_v = Cars(car_id).vx;
%		Cars(car_id).x = Cars(car_id).x +dist;
    end
    Cars(car_id).x = Cars(car_id).x +dist;

       [dr, bs] = get_sinr(Cars, car_id,Xb,Yb,Xrb,Yrb,NRF,NTHzT);
        ho_flag = 1;
        prev_bs = Cars(car_id).prev_bs;


        if tele_action == 8 % 1 0 0 % 1 1 0 % 1 1 1 
            ho_flag = 1;
            bs_candidate = [bs(1), bs(2),bs(3)]; % top 3 base station id
            dr_candidate = [dr(1), dr(2),dr(3)]; % top 3 base station data rate
            noc = number_of_connected(bs_candidate, Cars); 
            quo = quota(bs_candidate);

            coefficient = ones(1, length(bs_candidate)) ./ min([noc, quo]); % /number_of_connected(bs_candidate, Cars);%
            hop = ho_penalty(bs_candidate,prev_bs);
            dr_withquota = dr_candidate .* coefficient .*(1-hop);
%             dr_withquota = dr_candidate .* coefficient .*(1-ho_prob);
            [dr_sort, dr_sort_index] = sort(dr_withquota);
            dr_sort =fliplr(dr_sort);
            dr_sort_index =fliplr(dr_sort_index);
            Cars(car_id).max_bs = bs_candidate(dr_sort_index(1));%index1
            Cars(car_id).bs1 =  dr_sort(1);
            Cars(car_id).bs2 =  dr_sort(2);
            Cars(car_id).bs3 =  dr_sort(3);

        elseif tele_action == 9 % base station switch for bad connection (bs_c = 2)
%             ho_flag = 0;
            bs_candidate = [bs(1), bs(2),bs(3)]; % top 3 base station id
            dr_candidate = [dr(1), dr(2),dr(3)]; % top 3 base station data rate
            noc = number_of_connected(bs_candidate, Cars); 
            quo = quota(bs_candidate);

            coefficient = ones(1, length(bs_candidate)) ./ min([noc, quo]); % /number_of_connected(bs_candidate, Cars);%
             hop = ho_penalty(bs_candidate,prev_bs);
             
            dr_withquota = dr_candidate .* coefficient .*(1-hop);
            
%             dr_withquota = dr_candidate .* coefficient .*(1-ho_prob);
            [dr_sort, dr_sort_index] = sort(dr_withquota);
            dr_sort =fliplr(dr_sort);
            dr_sort_index =fliplr(dr_sort_index);
            Cars(car_id).max_bs = bs_candidate(dr_sort_index(1));%index1
            Cars(car_id).bs1 =  dr_sort(1);
            Cars(car_id).bs2 =  dr_sort(2);
            Cars(car_id).bs3 =  dr_sort(3);
        
        elseif tele_action == 10  %  no handover, keep the previous BS for this timestep.
            ho_flag = 0;
            prev_bs = Cars(car_id).prev_bs;
            % For the first time step, we do not have pre_bs.

            if time == 1 ||  ~ isstring(prev_bs) % strcmp(Cars(car_id).max_bs,"0") || Cars(car_id).bs1 == 0

                bs_candidate = [bs(1), bs(2),bs(3)]; % top 3 base station id
                dr_candidate = [dr(1), dr(2),dr(3)]; % top 3 base station data rate
                noc = number_of_connected(bs_candidate, Cars); 
                quo = quota(bs_candidate);
                coefficient = ones(1, length(bs_candidate)) ./ min([noc, quo]); % /number_of_connected(bs_candidate, Cars);%    
                
                dr_withquota = dr_candidate .* coefficient;

                  Cars(car_id).bs1 = dr_withquota(1);
                  Cars(car_id).bs2 = dr_withquota(2);
                  Cars(car_id).bs3 = dr_withquota(3);
                  Cars(car_id).max_bs = bs_candidate(1);

            else
                 
                 
                 Cars(car_id).max_bs = prev_bs;
                 fprintf("prev_bs,%s",prev_bs)
                 target_bs_id =  find_bs_index(bs, prev_bs) ;% find(bs,prev_bs);
                 fprintf("target_bs_id,%s",target_bs_id)
                 Cars(car_id).bs1 = dr(target_bs_id);

                bs_candidate = [bs(1), bs(2),bs(3)]; % top 3 base station id
                dr_candidate = [dr(1), dr(2),dr(3)]; % top 3 base station data rate
                noc = number_of_connected(bs_candidate, Cars); 
                quo = quota(bs_candidate);

                coefficient = ones(1, length(bs_candidate)) ./ min([noc, quo]); % /number_of_connected(bs_candidate, Cars);%    
                dr_withquota = dr_candidate .* coefficient;
                  
                Cars(car_id).bs2 = dr_withquota(2);
                  Cars(car_id).bs3 = dr_withquota(3);
    
            end
     
        
           
        end

        if car_id == 1
            dr_ego = Cars(car_id).bs1;
		    if ( not(strcmpi(Cars(car_id).max_bs, Cars(car_id).prev_bs)) && ho_flag)
                ho = ho + 1;
                 % update the base station
                %disp("hand off plus")
            end
             Cars(car_id).prev_bs = Cars(car_id).max_bs;
        end
        


end % update position: x and y

	 




return

function result = number_of_connected(bs_candidate, Cars)

result = zeros(1, length(bs_candidate));
ind = 1;
for member = bs_candidate
    s = string(member);
    str = string([Cars.('max_bs')]);
    bs_count = count(s,str );
    result(ind) = bs_count + 1; % avoid infinity
    ind = ind + 1;
end

end

function result = quota(bs_candidate)

result = zeros(1, length(bs_candidate));

ind = 1;
for member = bs_candidate
%     disp(['the bs is ' member])
%     s = string(member);
    b = char( member );
    if (strcmp(b(1), 'r'))
        result(ind) = 2;
    else %string(member).get(0) == 't'
        result(ind) = 5;
    end
    ind = ind + 1;
end
end

function ho_penalty = ho_penalty(bs_candidate,prev_bs)

ho_penalty = zeros(1, length(bs_candidate));
ind = 1;
for member = bs_candidate
%     disp(member{1})
    b = char( member );
    if ( strcmp( member,prev_bs) == true)
        ho_penalty(ind) = 0;
    else %string(member).get(0) == 't'
        if (strcmp(b(1), 't'))
            ho_penalty(ind) = 0.5;
        else
            ho_penalty(ind) = 0.1;
        end
        
    end
    ind = ind + 1;

end
end

function result = find_bs_index( bs_set,bs)
ind = 1;
for member = bs_set
    s = string(member);
    if ( strcmp( s,bs) == true)
        break;
    else
        ind = ind +1;
    end
 
end
result = ind;

end

end

