function car = Construct_car(pos_x, pos_y, vel_x, vel_y, accel, lane_id,bs11,bs22,bs33,max_bs,prev_bs)

% Construct structure of a single car
% pos_y = {0, 3.6}

setGlobal;

car.x = pos_x;
car.theta = pos_x / Params.road_radius;
car.y = pos_y;
car.vx = vel_x;
car.vy = vel_y;
car.a = accel;
car.lane_id = lane_id;
car.bs1 = bs11;
car.bs2 = bs22;
car.bs3 = bs33;
car.max_bs = max_bs;
car.prev_bs= prev_bs;