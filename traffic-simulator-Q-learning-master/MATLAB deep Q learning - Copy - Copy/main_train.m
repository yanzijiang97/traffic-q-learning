clear,close all,clc

% cd Parameters/
% Q_0 = csvread('Q_alpha_0.2_episode_10000_w5_1.csv');
% cd ..

policy1=csvread('Policy_1_92v20w6_alpha_0.005_episode_10000_test_with_ho_room.csv');


setting_str = '31v_30deep_episode_3000_w5_1_test_21transmissiont';
episode_limit = 5000;
time_limit    = 8*3600;     % in [seconds]
plot_flag     = false;  % usually not plot during training
epsilon       = 0.1;      % eps-greedy
if_eps_decay  = true;   % if anneal epsilon previously false
decay_rate    =  20;      % stop episode 2000
gamma         = 0.99;    % discount factor


net_out=train_Q(setting_str, episode_limit, time_limit, plot_flag,...
        epsilon, if_eps_decay, decay_rate, gamma,policy1);
