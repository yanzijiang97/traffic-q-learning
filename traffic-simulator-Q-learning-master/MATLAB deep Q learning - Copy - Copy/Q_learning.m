% lisheng 12/4/17
% This is actually a simulator

function [net_out, score,ho_density,DR_average,Average_Speed,...
    collision_flag,transition_store,ra_store,replay_count,wb_out,average_telecome_reward,average_transport_reward,...
    average_Dr_per_meter,time,transport_reward_total,telecome_reward_total,ho_prob]= ...
    Q_learning(net, num_env_cars, plot_flag, epsilon, gamma, neuron1,B,transition_store,ra_store,replay_count,wb,replay_size,net1,episode,policy1)
%% function [Q, score,ho_density,DR_average,R_transportation,R_telecome,R,Average_Speed,v_array_q,ho_array_q,ho_density_array_q, collision_flag,average_telecome_reward,average_transport_reward,average_Dr_per_meter,time,transport_reward_total,telecome_reward_total,ho_prob] ... 


setGlobal;

%% initialization
% Initizlie and Construct the traffic environment
Cars = simulator_initializaiton(Params, num_env_cars);


time = 0;
dist = 0;
score = 0;
ho_prob = 0;

HO_total = 0;
ho_density = 0;
DR_average = 0;
Average_Speed = 0;
% ho_prob = 0;

transport_reward_total = 0 ;
telecome_reward_total = 0 ;
average_telecome_reward = 0;
average_transport_reward =0;
average_Dr_per_meter = 0;





ego_obs = get_Observation(1, Cars, Params);
collision_flag = check_collision(Cars, Params);



NTHzT = 10;%5
NRF = 5;
R_max = 500;%1000 Params.nominal_speed * Params.horizon
road_length = 1400 ;


% Xb = R_max * rand(NTHzT,1)-250;
% Xb = -250:R_max/NTHzT:250-1;

Xb = 0:road_length/NTHzT:road_length-1;
Yb = 20 * logical(randi(2, [1 NTHzT]) - 1) -10;
Yb = Yb';

% Xrb = R_max * rand(NRF,1)-250;
% Xrb = -250:R_max/NRF:250-1;
Xrb = 0:road_length/NRF:road_length-1;
Yrb = 20 * logical(randi(2, [1 NRF]) - 1) -10;
Yrb = Yrb';



if plot_flag
    Plot_Traffics_not_saving_gif(Cars, time, dist, Params, ego_obs,Xb,Yb,Xrb,Yrb);
end

%% Run with dynamics for a given time horizon
% beta version, ego car uses policy 0

for time = 1 : Params.time_step : Params.horizon
    %% update dynamics, with relative coord, i.e. ego_x = 0;
    s = obs2state(ego_obs, Params);
    ego_obs_t = ego_obs;
%     R = get_reward(ego_obs, Cars(1), collision_flag, Params);
    
    % update Cars
%     function [Cars, dist, ego_action, ho, dr_ego, ego_v] = Q_update_dynamics(Cars, dist, Params, Q, epsilon,Xb,Yb,Xrb,Yrb,NRF,NTHzT,ho_prob,time)
    [Cars, dist, ego_action,ho,~,~] = Q_update_dynamics(Cars, dist, Params, net, epsilon,policy1,episode,Xb,Yb,Xrb,Yrb,NRF,NTHzT,ho_prob,time);
    
    
    HO_total = HO_total +ho;
	
    % check if there is collision:
    collision_flag = check_collision(Cars, Params);
    
    % update ego_obs
    % ego_obs of next time step:
    ego_obs = get_Observation(1, Cars, Params);
    [R_transportation,R_telecome,R]  = get_reward(ego_obs_t, Cars(1), collision_flag, Params,ho_prob);
   
    transport_reward_total = transport_reward_total + R_transportation;
    telecome_reward_total = telecome_reward_total + R_telecome;

    a = ego_action;
    % update state to next state
    s_n = obs2state(ego_obs, Params);
    
   %% 
    if collision_flag == false
   
        replay_count=replay_count+1;
        %update replay memory
%         disp('state_trf_s')
%         disp(stste_trf(s));
%         disp(transition_store(:,replay_count,1))
        transition_store(:,replay_count,1)=stste_trf(s);
        transition_store(:,replay_count,2)=stste_trf(s_n);
        ra_store(1,replay_count)=R;
        ra_store(2,replay_count)=a;
        if replay_count==replay_size    
        replay_count=0;
        end
  
        if size(ra_store,2)>=B && episode>50 % if replay memory size is larger than mini batch size  
                    
            batch_index=randperm(size(ra_store,2),B);          
            r_p=ra_store(1,batch_index); % reward=1*B
            a_p=ra_store(2,batch_index); % action =1*B
            s_p=transition_store(:,batch_index,1); % mini batch s sample in cloumn
            s_np=transition_store(:,batch_index,2); % mini batch s'
            
            q=net(s_p);% current predicted q(s) in cloumn
            q_n=net1(s_np);
            update_q=r_p+gamma*max(q_n);
        
         
            update_index=sub2ind(size(q),a_p,1:B);
            q(update_index)=update_q; % new q for training
            
            
            %net2 = feedforwardnet([neuron1,neuron2]); %initialize neural network
            net2 = feedforwardnet([neuron1,neuron1,neuron1]); %initialize neural network
            net2.performFcn = 'mse';
            net2.trainFcn = 'traingdm';
            net2.divideParam.trainRatio = 1;
            net2.divideParam.valRatio = 0;
            net2.divideParam.testRatio = 0;
            net2.trainParam.epochs=1;
            net2.trainParam.showWindow=false;
            net2.layers{1}.transferFcn = 'poslin';
            net2.layers{2}.transferFcn = 'poslin';
            net2.layers{3}.transferFcn = 'poslin';
            net2.layers{4}.transferFcn = 'purelin';
            net2 = setwb(net2,wb);
                
            net_out = train(net2,s_p,q); % new neural netwrok      
            
            wb_out=getwb(net_out);
            
        else
             net_out=net;
             wb_out=wb;
        end
        
        
        
        
        
        if plot_flag
            Plot_Traffics_not_saving_gif(Cars, time, dist, Params, ego_obs,Xb,Yb,Xrb,Yrb);
        end
    else
        % filter bad initialization
        if time >= 3
            
           
        replay_count=replay_count+1;
        %update replay memory
              
        transition_store(:,replay_count,1)=stste_trf(s);
        transition_store(:,replay_count,2)=stste_trf(s_n);
        ra_store(1,replay_count)=R;
        ra_store(2,replay_count)=a;
        if replay_count==replay_size    
        replay_count=0;
        end
        if size(ra_store,2)>=B && episode>50 % if replay memory size is larger than mini batch size
            
           
            
            batch_index=randperm(size(ra_store,2),B);          
            r_p=ra_store(1,batch_index); % reward=1*B
            a_p=ra_store(2,batch_index); % action =1*B
            s_p=transition_store(:,batch_index,1); % mini batch s sample in cloumn
            s_np=transition_store(:,batch_index,2); % mini batch s'
            
            q=net(s_p);% current predicted q(s) in cloumn
            q_n=net1(s_np);
            update_q=r_p+gamma*max(q_n);
        
         
            update_index=sub2ind(size(q),a_p,1:B);
            q(update_index)=update_q; % new q for training
            
            
            %net2 = feedforwardnet([neuron1,neuron2]); %initialize neural network
            
            net2 = feedforwardnet([neuron1,neuron1,neuron1]); %initialize neural network
            net2.performFcn = 'mse';
            net2.trainFcn = 'traingdm';
            net2.divideParam.trainRatio = 1;
            net2.divideParam.valRatio = 0;
            net2.divideParam.testRatio = 0;
            net2.trainParam.epochs=1;
            net2.trainParam.showWindow=false;
            net2.layers{1}.transferFcn = 'poslin';
            net2.layers{2}.transferFcn = 'poslin';
            net2.layers{3}.transferFcn = 'poslin';
            net2.layers{4}.transferFcn = 'purelin';
            net2 = setwb(net2,wb);
                
            net_out = train(net2,s_p,q); % new neural netwrok      
            
            wb_out=getwb(net_out);
            
        else
             net_out=net;
             wb_out=wb;
        end
            
            
            
            
            
            score = (dist-4000)/1000;
            ho_prob = min(1,HO_total/time);
            Average_Speed  = dist/time;
            average_telecome_reward = telecome_reward_total/time;
            average_transport_reward = transport_reward_total/time;


            disp('Collision!');
            fprintf('Distance Travelled = %3.4d m\n', dist);
            fprintf('Average Speed      = %3.2f m/s\n', dist/time);
            fprintf('Score              = %3.2f \n', score);
			fprintf('HO  prob           = %3.2f \n', ho_prob);
            fprintf('Average telecome reward              = %8.2f \n', average_telecome_reward);
            fprintf('Average transport reward              = %8.2f \n', average_transport_reward);

            if plot_flag
                Plot_Traffics_not_saving_gif(Cars, time, dist, Params, ego_obs,Xb,Yb,Xrb,Yrb);
            end
        else
             net_out=net;
             wb_out=wb;
            disp('Bad initialization!');
        end
        
        break; % break the current episode & restart simulation
        
    end % collision check
    
end % for time
if collision_flag == false
    score = (dist-4000)/1000;
    ho_density = HO_total/dist ; % instant velocity  * dist/time
    Average_Speed  = dist/time;
    average_telecome_reward = telecome_reward_total/time;
    average_transport_reward = transport_reward_total/time;
    ho_prob = min(1,HO_total/time);


    disp('No Collision');
    fprintf('Distance Travelled = %3.4d m\n', dist);
    fprintf('Average Speed      = %3.2f m/s\n', dist/time);
    fprintf('Score              = %3.2f \n', score);
    fprintf('HO total                = %3.2f \n', HO_total);
    fprintf('Average telecome reward              = %8.2f \n', average_telecome_reward);
    fprintf('Average transport reward              = %8.2f \n', average_transport_reward);
	fprintf('HO  prob           = %3.2f \n', ho_prob);

end



return



