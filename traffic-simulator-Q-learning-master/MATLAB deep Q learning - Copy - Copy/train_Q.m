
function net_out=train_Q(setting_str, episode_limit, time_limit, plot_flag,...
    epsilon, if_eps_decay, decay_rate, gamma,policy1)

%%Q learning set up

score = zeros(episode_limit, 1);
ho_density = zeros(episode_limit, 1);
DR_average = zeros(episode_limit, 1);



average_telecome_reward= zeros(episode_limit, 1);
average_transport_reward=zeros(episode_limit, 1);
average_total_reward=zeros(episode_limit, 1);
transport_reward_total = zeros(episode_limit,1);
telecome_reward_total = zeros(episode_limit,1);
time = zeros(episode_limit,1);
average_Dr_per_meter = zeros(episode_limit,1);
ho_prob = zeros(episode_limit,1);


Average_Speed = zeros(episode_limit, 1);

K = decay_rate / episode_limit;






%%

net_update=50;
replay_size=100000;

B=128; %mini batch size
neuron1=8; % # neuron
NEURON2 = [8,8,8];

n_statev= 8; % state variable
n_action= 21; % action space 6

s_i=randi(8,n_statev,B); % configure input size
q_i=rand(n_action,B);% configure output size

transition_store=zeros(n_statev,1,2);% first layer s second layer s'
ra_store=zeros(2,1); % first row reward second row action
replay_count=0;

% net = feedforwardnet([neuron1,neuron2]); %initialize neural network
net = feedforwardnet(NEURON2); %initialize neural network
net = configure(net,s_i,q_i); %initialize neural network
net.performFcn = 'mse';
net.trainFcn = 'traingdm';
net.divideParam.trainRatio = 1;
net.divideParam.valRatio = 0;
net.divideParam.testRatio = 0;
net.trainParam.epochs=1;
net.trainParam.showWindow=false;
net.layers{1}.transferFcn = 'poslin';
net.layers{2}.transferFcn = 'poslin';
net.layers{3}.transferFcn = 'poslin';
net.layers{4}.transferFcn = 'purelin';

wb=getwb(net);

net1=net;
net_update_counter=0;
net_out=net;
wb_out=wb;




% max_states_num = 3^6 * 2 *3;
% action_num = 21;


score = zeros(episode_limit, 1);

% K = 9/decay_rate ;

K = decay_rate / episode_limit;

start_time = tic;
for episode = 1 : episode_limit

    % epsilon anealing
%     if if_eps_decay && episode>2000 && (episode-2000)<=decay_rate
%         epsilon = 1 /(1+ K * (episode-2000));
%         if episode-2000>decay_rate
%             epsilon=0.1;
%         end
%     end

    if if_eps_decay
        epsilon = 1 * exp(- K * episode);
    end


    fprintf('\nepisode = %d\n', episode);
    disp(epsilon)
    num_env_cars = randi(5) + 20;

    net_in=net_out;
    wb_in=wb_out;
    
    if episode>50
    net_update_counter=net_update_counter+1;
    end
    if net_update_counter==net_update
        net1=net_in;
        net_update_counter=0;
    end
    

%     function [net_out, score,ho_density,DR_average,Average_Speed,...
%     collision_flag,transition_store,ra_store,replay_count,wb_out,average_telecome_reward,average_transport_reward,...
%     average_Dr_per_meter,time,transport_reward_total,telecome_reward_total,ho_prob]= ...
%     Q_learning(net, num_env_cars, plot_flag, epsilon, gamma, neuron1,B,transition_store,ra_store,replay_count,wb,replay_size,net1,episode,policy1)

  [net_out, score(episode),ho_density(episode),DR_average(episode),Average_Speed,...
    collision_flag,transition_store,ra_store,replay_count,wb_out,average_telecome_reward(episode),average_transport_reward(episode),...
    average_Dr_per_meter(episode),time(episode),transport_reward_total(episode),telecome_reward_total(episode),ho_prob(episode)]= ...
    Q_learning(net_in, num_env_cars, plot_flag, epsilon, gamma, neuron1,B,transition_store,ra_store,replay_count,wb_in,replay_size,net1,episode,policy1);

    
    plot_y(score(episode), episode, collision_flag, epsilon,'score',1); 
    plot_y(average_telecome_reward(episode), episode, collision_flag, epsilon,'average telecome reward',5);


    average_total_reward(episode)=average_telecome_reward(episode)+average_transport_reward(episode);



 
    
    if mod(episode, 500) == 0

        s1 = int2str(episode);
        s2 = '_episode_';
        s = strcat(s2,s1);
        
        set_str = strcat(s,setting_str);

        
        save net_out
        for i=1:1:3^7*2
            [~,a]=max(net_out(stste_trf(i)));
            policy(i,:)=a;
        end
        

		cd Parameters
        csvwrite(['score_' set_str '.csv'], score);
        csvwrite(['ho_density_' set_str '.csv'], ho_density);
        csvwrite(['speed_average_' set_str '.csv'], Average_Speed);
        csvwrite(['tele_reward_average_' set_str '.csv'], average_telecome_reward);
        csvwrite(['tran_reward_average_' set_str '.csv'], average_transport_reward);
        csvwrite(['total_reward_average_' set_str '.csv'], average_total_reward);
		csvwrite(['time_' set_str '.csv'], time);
		csvwrite(['ho_prob_' set_str '.csv'], ho_prob);

        cd ..
    end
    
    if toc(start_time) > time_limit
        break;
    end
    
    if plot_flag == true
        close all;
    end
    
end % for episode

save net_out

for i=1:1:3^6*2
    [~,a]=max(net_out(stste_trf(i)));
    policy(i,:)=a;
end


cd Parameters

csvwrite(['score_' setting_str '.csv'], score);
csvwrite(['ho_density_' setting_str '.csv'], ho_density);
csvwrite(['average_speed_' setting_str '.csv'], Average_Speed);
csvwrite(['tele_reward_average_' setting_str '.csv'], average_telecome_reward);
csvwrite(['tran_reward_average_' setting_str '.csv'], average_transport_reward);
csvwrite(['total_reward_average_' setting_str '.csv'], average_total_reward);
csvwrite(['time_' setting_str '.csv'], time);
csvwrite(['ho_prob_' setting_str '.csv'], ho_prob);

cd ..




figure;
