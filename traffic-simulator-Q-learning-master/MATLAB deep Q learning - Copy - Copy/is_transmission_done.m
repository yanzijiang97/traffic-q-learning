function transmission_flag = is_transmission_done(DR_total,Params)
transmission_flag = false;


if DR_total > (Params.horizon-22) * 2e8
    transmission_flag = true;
end

return