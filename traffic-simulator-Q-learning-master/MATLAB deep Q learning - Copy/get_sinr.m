function [dr,bs] = get_sinr(ego_car,bslist,place)

%% =================== RF Parameters ======================================
alpha =2.5;                       %Path Loss Exponent better to have indoor small
PR = 1;
fcRF=2.1*10^9;
GT=1;
GR=1;
gammaI=(3*10^8)^2*GT*GR/(16*pi^2*fcRF^2); %used in sinr 
 
%% ===================== THz Parameters ===========================
PT = 1;                             % Tranmitted Power
kf = 0.05;                           % Absorbtion loss
fcTH=1.0*10^12;
GTT=316.2;
GRR=316.2;
% GTT=31.62;
% GRR=31.62;

thetabs=pi/6;%%%in degrees
thetamt=pi/6;%%%in degrees
FBS=thetabs/(2*pi);
FMT=thetamt/(2*pi);
prob= FBS*FMT;
gammaII=(3*10^8)^2*GTT*GRR/(16*pi^2*fcTH^2);
 

R_max = 100;
Nit = 10000;
%lambdas=10;
%% ===================== Rate and SINR theshold calculation ========================================
Rate = [5 ]*10^9;
Wt=5*10^8;
Wr=40*10^6;
SINRthRF =2.^(Rate./(Wr))-1; %thresholds
SINRthTH =2.^(Rate./(Wt))-1;

Bias=[1000 100 1 0.001 0.0001 0.0001 0.00001];%%%0.05
%Bias=[ 10^6  10^5 10^4  10^4 10^3 10^3 10^3];%%%0.2

%if type == 'RF'
    
%    sinr = 1
   
      for i=1:200%Nit
        
        UoI = 1;
        lambda=2;
        % The index of User of Interest
        NRF = 5;
        NTHzT = 5;
        Nu = 30;
  
        
        fadeRand = exprnd(1,NRF,UoI);
        
      
%             rue = R_max*sqrt(rand(Nu,1));
%             thetau = 2*pi*rand(Nu,1);
%             Xu = rue.*cos(thetau);
%             Yu = rue.*sin(thetau);

            Xu = ego_car.x;
            Yu = ego_car.y;
            
            
            rTHzbs = R_max*sqrt(rand(NTHzT,1));
            thetaTb = 2*pi*rand(NTHzT,1);
            Xb = rTHzbs.*cos(thetaTb);
            Yb = rTHzbs.*sin(thetaTb);
            
            rRFzbs = R_max*sqrt(rand(NRF,1));
            thetaRb = 2*pi*rand(NRF,1);
            Xrb = rRFzbs.*cos(thetaRb);
            Yrb = rRFzbs.*sin(thetaRb);
            
            
            % Distacnces from THz BSs
            
            [Xmp_Tmat, Xp_Tmat] = meshgrid(Xu,Xb);
            [Ymp_Tmat, Yp_Tmat] = meshgrid(Yu,Yb);
            D_ue_Tbs = sqrt((Xmp_Tmat-Xp_Tmat).^2 + (Ymp_Tmat-Yp_Tmat).^2);
            
            % Distacnces from RF BSs
            
            [Xmp_Rmat, Xp_Rmat] = meshgrid(Xu,Xrb);
            [Ymp_Rmat, Yp_Rmat] = meshgrid(Yu,Yrb);
            D_ue_Rbs = sqrt((Xmp_Rmat-Xp_Rmat).^2 + (Ymp_Rmat-Yp_Rmat).^2);
            
            fadeRand = exprnd(1,NRF,Nu);
            SRF=gammaI.*fadeRand.*PR.*D_ue_Rbs.^(-alpha); %signal matrix for RF
            NP=(10)^-10;
            interf=repmat(sum(SRF,1),NRF,1)-SRF; %interference for RF
            RPrAllu1 = log2(1+SRF./(NP+interf)); %power from all base-stations to all users
%             RPrAllu = log2(1+SRF./NP);
            
            
            
            fadeRand1 = exprnd(1,NTHzT,Nu);
            STHz=gammaII.*fadeRand1.*PT.*exp(-kf.*D_ue_Tbs)./(D_ue_Tbs.^2); %signal matrix for THZ
            interfT=repmat(sum(STHz,1),NTHzT,1)-STHz; %interference matrix for THz
            TPrAllu1 =log2(1+STHz./(NP+interfT));
           % TPrAllu =log2(1+STHz./(NP));
            
%             SINR_Matrix = [RPrAllu1;TPrAllu1];
%            disp(RPrAllu1)
%            disp(TPrAllu)
            
            %  return 3 maximum sinr in RF BS
           [RF_max, RF_index] = maxk(RPrAllu1, 3, 2);% 1
           RF_ego = RF_max(1,:);
           RF_index = RF_index(1,:);
           RF_index =  'r' + string(RF_index);

%            B = maxk(RF_max,3,2)
%            [~,RF] = maxk(RPrAllu1,3); % find top 3 index
%            RF = RF+'r';
           
            %  return 3 maximum sinr in THz BS
            [THz_max, THz_index] = maxk(TPrAllu1, 3, 2);% 1
            Thz_ego = THz_max(1,:);
            THz_index = THz_index(1,:);
            THz_index =  't' + string(THz_index);
%             THz_index = num2str(THz_index);
        
%            keySet = {'Jan', 'Feb', 'Mar', 'Apr'};
%            valueSet = [327.2, 368.2, 197.6, 178.4];
%            mapObj = containers.Map(keySet,valueSet);
%            keySet = {}
%            valueSet = {}
%            for i = 1:3
% %                 keySet = [keySet,RF(i)+'r'];
%                 valueSet = [valueSet;RF_max(i)]
%            end
%            
%            for i = 1:3
% %                 keySet = [keySet,Thz(i)+'i'];
%                 valueSet = [valueSet;Thz_max(i)]
%            end
           
           keySet = union(RF_index,THz_index);
           valueSet = union(RF_ego,Thz_ego);
%            mapObj = containers.Map(keySet,valueSet);
           mapObj = containers.Map(keySet,valueSet);
           
           keys = mapObj.keys;
           values = mapObj.values;
           Str = sprintf('%s,', valueSet);%{:}
           D = sscanf(Str, '%g,');
           [dummy, index] = sort(D);
           sortedkey = keys(index);
           sortedValue = values(index);
           
           sortedkey = vertcat(sortedkey);%format {:}
           sortedValue = vertcat(sortedValue{:});
%             VALUE_SET = maxK(SINR_Matrix, [], 3);% 1 
%             [~,INDEX_set] = maxk(keySet,3); % find top 3 index
%             
%             % return 3 maximum sinr
%             [SINR_max, i] = max(SINR_Matrix, [], 3);% 1 
%             [~,index_SET] = maxk(SINR_Matrix,3); % find top 3 index
%             
            %%%%%% store the type of base station here
            
            dr_max1 = sortedValue(length(sortedValue));
            dr_max2 = sortedValue(length(sortedValue)-1);
            dr_max3 = sortedValue(length(sortedValue)-2);
            
            b1 = sortedkey(length(sortedkey));
            b2 = sortedkey(length(sortedkey)-1);
            b3 = sortedkey(length(sortedkey)-2);
            
            %data_max1 = 
            %data_max2 = 
            %data_max3 = 
            
            
            dr = [dr_max1,dr_max2, dr_max3];
            %sinr = [b1,b2,b3]
            bs = [b1,b2, b3];
            
            %SINR_sum(i) = sum(SINR_max); %benchmark for max SINR
            
            
      end










% if rand()<0.3
%     sinr = [1,122,2,333,3,114] 
% elseif rand() <0.6
%     sinr = [1,222,2,333,3,100]
% else
%     sinr = [1,333,2,211,3,105]
    
    



end
