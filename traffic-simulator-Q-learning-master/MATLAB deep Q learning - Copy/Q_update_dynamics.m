function [Cars, dist, ego_action, ho, dr_ego] = Q_update_dynamics(Cars, dist, Params, net, epsilon,policy1,episode)
dt = Params.time_step;
action_num = 9; %6
ho = 0;

% get current observation of each car
for car_id = 1:length(Cars)
    obs_of_all_cars(car_id) = get_Observation(car_id, Cars, Params);
    %     Cars(car_id).max_bs = obs_of_all_cars(car_id).best_bs;
    %% telecommunication side
    [dr, bs] = get_sinr(Cars, car_id);
    %bs2 = get_sinr(subject_car,2)(2);
    %bs3 = get_sinr(subject_car,3)(3);
    Cars(car_id).bs1 = dr(3);
    Cars(car_id).bs2 = dr(2);
    Cars(car_id).bs3 = dr(1);
 
    Cars(car_id).max_bs = bs(3);
 
    %     best_bs = bs(3);
 
end

% update position: x and y
for car_id = 1:length(Cars)
    [dr, bs] = get_sinr(Cars, car_id);
    if car_id ~= 1 % env car action selection
        action_id = policy_0(obs_of_all_cars(car_id), Params);
     
        dr_max = max([dr(1), dr(2), dr(3)]);
     
        if action_id < 7 % ~= action_id == 1 is for 0 0 0 case
            dynamics = act2dyn(action_id, Params);
            acc = dynamics(1);
            Cars(car_id).vy = dynamics(2);
         
        elseif action_id == 7 % collision
            acc = - Cars(car_id).vx / dt * 0.85;
            Cars(car_id).vy = 0;
         
        elseif action_id == 8 % 1 0 0 date rate only
            acc = 0;
            Cars(car_id).vy = 0;
            if dr_max == dr(1)
                Cars(car_id).max_bs = bs(3);
                Cars(car_id).bs1 = dr(1);
                break
            elseif dr_max == dr(2)
                Cars(car_id).max_bs = bs(2);
                Cars(car_id).bs1 = dr(2);
                break
            elseif dr_max == dr(3)
                Cars(car_id).max_bs = bs(1);
                Cars(car_id).bs1 = dr(3);
                break
            end
         
        elseif action_id == 9
            % 1 1 0(sorted) 1 0 1 (unsorted)
            % max 2nd max data rate
            % Thz RF check number of vehiles here.
            % multiply 1 1 0  check number of
            % quota calculating data rate
            % combine action id is 9 10
            acc = 0;
            Cars(car_id).vy = 0;
            bs_candidate = [bs(3), bs(2)]; % base station id
            dr_candidate = [dr(1), dr(2)]; % data rate
            %             quota_rf = 2;
            %             quota_Thz = 5;
         
            %TO BE IMPLEMENTED num of connections > quota in terms of bs,
            %coefficient = 1/quota, in terms of car, the coefficient 0
         
            %             number_of_connecte = number_of_connected(bs_candidate);
            %             quota_threshold
         
            %%%%%
            %           n =  number_of_connected(bs_candidate,Cars);
            %           m = quota(bs_candidate);
%             coefficient = ones(1, length(bs_candidate)) ./min([number_of_connected(bs_candidate, Cars), quota(bs_candidate)]);%min([number_of_connected(bs_candidate, Cars), quota(bs_candidate)]);number_of_connected(bs_candidate, Cars); 
            coefficient = 1;
            dr_withquota = dr_candidate .* coefficient;
         
            if dr_withquota(1) > dr_withquota(2)
                Cars(car_id).max_bs = bs(3);
                Cars(car_id).bs1 = dr(1);
             
            else
                Cars(car_id).max_bs = bs(2);
                Cars(car_id).bs1 = dr(2);
             
            end
         
            %         elseif action_id == 10 % collision
            % %             dr_max = max(dr(1),dr(2),dr(3));
            %
            %             if  dr_max == dr(1)
            %                 Cars(car_id).max_bs = bs(3);
            %                 Cars(car_id).bs1 = dr(1);
            %                 break
            %             elseif dr_max == dr(2)
            %                 Cars(car_id).max_bs = bs(2);
            %                 Cars(car_id).bs1 = dr(2);
            %                 break
            %             elseif dr_max == dr(3)
            %                 Cars(car_id).max_bs = bs(1);
            %                 Cars(car_id).bs1 = dr(3);
            %                 break
            %             end
         
            if (strcmpi(Cars(car_id).max_bs, Cars(car_id).prev_bs) == false)
                %             ho = ho + 1;
                Cars(car_id).prev_bs = Cars(car_id).max_bs; % update the base station
                %disp("hand off plus")
            end
            %          else
            %             Cars(car_id).prev_bs = Cars(car_id).max_bs;
            %          end
         
        end

    else % ego car action selection
     
        ego_state = obs2state(obs_of_all_cars(car_id), Params);
     
        if rand() <= epsilon
            rand1 = randi(action_num);
         
            while (rand1 == 7)
                rand1 = randi(action_num); %avoid random number to stop state
            end
         
            ego_action = rand1;
        else
            disp(net.inputs{1}.size)
            [~, ego_action] = max(net(stste_trf(ego_state)));
         
        end
     
        dr_ego = max([Cars(car_id).bs1, Cars(car_id).bs2, Cars(car_id).bs3]);
     

            acc = 0;
            Cars(car_id).vy = 0;
        if ego_action < 7 % ~=
            dynamics = act2dyn(ego_action, Params);
            acc = dynamics(1);
            Cars(car_id).vy = dynamics(2);
         
%         elseif ego_action == 8 % 1 0 0
%             Cars(car_id).max_bs = bs(3);
%             Cars(car_id).dr = dr(1);
         
        elseif ego_action == 8 % 1 0 0 date rate only
            acc = 0;
            Cars(car_id).vy = 0;
            dr_max = dr_ego;
            if dr_max == dr(1)
                Cars(car_id).max_bs = bs(3);
                Cars(car_id).bs1 = dr(1);
                
            elseif dr_max == dr(2)
                Cars(car_id).max_bs = bs(2);
                Cars(car_id).bs1 = dr(2);
                
            elseif dr_max == dr(3)
                Cars(car_id).max_bs = bs(1);
                Cars(car_id).bs1 = dr(3);
                
            end
         
        elseif ego_action == 9
            % 1 1 0(sorted) 1 0 1 (unsorted)
            % max 2nd max data rate
            % Thz RF check number of vehiles here.
            % multiply 1 1 0  check number of
            % quota calculating data rate
            % combine action id is 9 10
            acc = 0;
            Cars(car_id).vy = 0;
         
            bs_candidate = [bs(3), bs(2)]; % base station id
            dr_candidate = [dr(1), dr(2)]; % data rate
         
            %TO BE IMPLEMENTED num of connections > quota in terms of bs,
            %coefficient = 1/quota, in terms of car, the coefficient 0
         
            %%%%%
            coefficient = ones(1, length(bs_candidate)) ./ min([number_of_connected(bs_candidate, Cars), quota(bs_candidate)]); % /number_of_connected(bs_candidate, Cars);%
         
            dr_withquota = dr_candidate .* coefficient;
         
            if dr_withquota(1) > dr_withquota(2)
                Cars(car_id).max_bs = bs(3);
                Cars(car_id).bs1 = dr(1);
             
            else
                Cars(car_id).max_bs = bs(2);
                Cars(car_id).bs1 = dr(2);
             
            end
            
        
            %         elseif action_id == 10 % collision
            % %             dr_max = max(dr(1),dr(2),dr(3));
            %
            %             if  dr_max == dr(1)
            %                 Cars(car_id).max_bs = bs(3);
            %                 Cars(car_id).bs1 = dr(1);
            %                 break
            %             elseif dr_max == dr(2)
            %                 Cars(car_id).max_bs = bs(2);
            %                 Cars(car_id).bs1 = dr(2);
            %                 break
            %             elseif dr_max == dr(3)
            %                 Cars(car_id).max_bs = bs(1);
            %                 Cars(car_id).bs1 = dr(3);
            %                 break
            %             end
         

            %         elseif ego_action == 9 % 1 1 0(sorted) 1 0 1 (unsorted)
            %
            %             if  dr_ego == dr(1)
            %                 Cars(car_id).max_bs = bs(3);
            %                 Cars(car_id).bs1 = dr(1);
            %
            %                 break
            %             elseif dr_ego == dr(2)
            %                 Cars(car_id).max_bs = bs(2);
            %                 Cars(car_id).bs1 = dr(2);
            %
            %                 break
            %             elseif dr_ego == dr(3)
            %                 Cars(car_id).max_bs = bs(1);
            %                 Cars(car_id).bs1 = dr(3);
            %
            %                 break
            %             end
            %
            %
            %         elseif ego_action == 10 % collision
            %             dr_ego = max([dr(1),dr(2),dr(3)]);
            %
            %             if  dr_ego == dr(1)
            %                 Cars(car_id).max_bs = bs(3);
            %                 Cars(car_id).bs1 = dr(1);
            %                 acc = 0;
            %                 Cars(car_id).vy = 0;
            %                 break
            %             elseif dr_ego == dr(2)
            %                 Cars(car_id).max_bs = bs(2);
            %                 Cars(car_id).bs1 = dr(2);
            %                 acc = 0;
            %                 Cars(car_id).vy = 0;
            %                 break
            %             elseif dr_ego == dr(3)
            %                 Cars(car_id).max_bs = bs(1);
            %                 Cars(car_id).bs1 = dr(3);
            %                 acc = 0;
            %                 Cars(car_id).vy = 0;
            %                 break
            %             end
            %
            %
        end
        if (strcmpi(Cars(car_id).max_bs, Cars(car_id).prev_bs) == false)
            ho = ho + 1;
            Cars(car_id).prev_bs = Cars(car_id).max_bs; % update the base station
            %disp("hand off plus")
        end
    end
 
    % update velocity using dynamics
    Cars(car_id).a = acc;
    Cars(car_id).vx = Cars(car_id).vx + acc * dt;
 
    % speed limit
    if Cars(car_id).vx > Params.max_speed
        Cars(car_id).a = 0; % Cars(car_id).a = (Cars(car_id).vx - Params.max_speed)/dt;
        Cars(car_id).vx = Params.max_speed;
    elseif Cars(car_id).vx < Params.min_speed
        Cars(car_id).a = 0;
        Cars(car_id).vx = Params.min_speed;
    end
 
    % update position using dynamics
    % x
    Cars(car_id).x = Cars(car_id).x + Cars(car_id).vx * dt;
    % y, update y and lane_id if vy ~= 0
    if Cars(car_id).vy ~= 0
        if Cars(car_id).lane_id == 1
            Cars(car_id).y = Cars(car_id).y + Cars(car_id).vy * dt;
            Cars(car_id).lane_id = 2;
        else
            Cars(car_id).y = Cars(car_id).y - Cars(car_id).vy * dt;
            Cars(car_id).lane_id = 1;
        end
    end
 
    % set relative x - coordinate (wrt ego car)
    Cars(car_id).x = Cars(car_id).x - Cars(1).vx * dt;
    Cars(car_id).theta = Cars(car_id).x / Params.road_radius;
 
    % re-regulate to -pi~pi
    if Cars(car_id).theta > pi
        Cars(car_id).theta = Cars(car_id).theta - 2 * pi;
    elseif Cars(car_id).theta < - pi
        Cars(car_id).theta = Cars(car_id).theta + 2 * pi;
    end
 
    Cars(car_id).x = Cars(car_id).theta * Params.road_radius;
 
    % calculate the distance ego car has travelled
    if car_id == 1
        dist = dist + Cars(car_id).vx * dt;
        %         [drr,bs] = get_sinr(Cars,car_id);%anglelist
        %         Cars(car_id).bs1 = drr(1);
        %         Cars(car_id).bs2 = drr(2);
        %         Cars(car_id).bs3 = drr(3);
        %         Cars(car_id).max_bs = bs(3);
    end
end % update position: x and y

return

function result = number_of_connected(bs_candidate, Cars)
% [3,4]
%length(bs_candidate) 2
result = zeros(1, length(bs_candidate));

ind = 1;
for member = bs_candidate
    %      disp(['the bs is ' member])
    s = string(member);
    %      bs_count = max(1,count(Cars.max_bs,s));
    % max_bssss = [Cars.('max_bs')];
    bs_counts = Cars([Cars.('max_bs')] == s, :);
    bs_count = max(1, length(bs_counts));
    result(ind) = bs_count;
    ind = ind + 1;
end

end

function result = quota(bs_candidate)

result = zeros(1, length(bs_candidate));

ind = 1;
for member = bs_candidate
    %      disp(['the bs is ' member])
    s = string(member);
    if (s(1) == 'r')
        result(ind) = 2;
    else %string(member).get(0) == 't'
        result(ind) = 5;
    end
    ind = ind + 1;
end
end

function result = handoff_prob(bs_candidate)

result = zeros(length(bs_candidate));

ind = 1;
for member1 = bs_candidate
    %      disp(['the bs is ' member1])
    if (string(member1).get(0) == 'r')
        result(ind) = 2;
    else %string(member).get(0) == 't'
        result(ind) = 5;
    end
    ind = ind + 1;
end

end
end