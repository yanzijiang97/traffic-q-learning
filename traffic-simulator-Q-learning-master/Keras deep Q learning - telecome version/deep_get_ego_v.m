function ego_v = deep_get_ego_v(dist,ego_action)
setGlobal;

[Cars, dist, ego_action, ho, dr_ego, ego_v] = deepQ_update_dynamics(dist,ego_action)

return;