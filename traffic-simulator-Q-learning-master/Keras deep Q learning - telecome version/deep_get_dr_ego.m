function dr_ego = deep_get_dr_ego(dist,ego_action)
setGlobal;

[Cars, dist, ego_action, ho, dr_ego, ego_v] = deepQ_update_dynamics(dist,ego_action)

return;