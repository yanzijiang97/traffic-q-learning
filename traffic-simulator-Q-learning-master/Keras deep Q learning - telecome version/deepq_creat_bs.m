function [Xb,Yb,Xrb,Yrb]=deepq_creat_bs() 

%SINR set up
NTHzT =5;
NRF = 5;
R_max = 500;%1000 Params.nominal_speed * Params.horizon
% rTHzbs = R_max*sqrt(rand(NTHzT,1));
% thetaTb = 2*pi*rand(NTHzT,1);
% Xb = rTHzbs.*cos(thetaTb);
% Yb = rTHzbs.*sin(thetaTb);

%generate random numbers from range for example [-250,250]
%Generate the y location in both sides of roads.
Xb = R_max * rand(NTHzT,1)-250;
Yb = 20 * logical(randi(2, [1 NTHzT]) - 1) -10;
Yb = Yb';

Xrb = R_max * rand(NRF,1)-250;
Yrb = 20 * logical(randi(2, [1 NRF]) - 1) -10;
Yrb = Yrb';