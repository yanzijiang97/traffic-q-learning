function ho = deep_get_ho(dist,ego_action)
setGlobal;

[~, ho] = deepQ_update_dynamics(dist,ego_action);

return;