function dist= deep_get_dist(dist,ego_action)
setGlobal;

[~, dist] = deepQ_update_dynamics(dist,ego_action);

return;