% lisheng 12/4/17
% This is actually a simulator

function [Q, score,ho_density,DR_average,R_transportation,R_telecome,R,Average_Speed,v_array_q,ho_array_q,ho_density_array_q, collision_flag, N] ... 
    = Q_learning(Q, N, num_env_cars, plot_flag, epsilon, gamma, alpha)
%%
setGlobal;

%% initialization
% Initizlie and Construct the traffic environment
Cars = simulator_initializaiton(Params, num_env_cars);


time = 0;
dist = 0;
score = 0;

HO_total = 0;
DR_total = 0;
ho_density = 0;
DR_average = 0;
Average_Speed = 0;

v_array_q = zeros(1,Params.horizon);
ho_array_q = zeros(1,Params.horizon) ; %100 is penalized factors
ho_density_array_q = ones(1,Params.horizon);

ego_obs = get_Observation(1, Cars, Params);
collision_flag = check_collision(Cars, Params);

if plot_flag
    Plot_Traffics_not_saving_gif(Cars, time, dist, Params, ego_obs);
end

%% Run with dynamics for a given time horizon
% beta version, ego car uses policy 0

for time = 1 : Params.time_step : Params.horizon %200 _>35 30m/s *200s =6000m 30m/s *35s =1050m  0-1000m 1000-6000
    %% update dynamics, with relative coord, i.e. ego_x = 0;
    s = obs2state(ego_obs, Params);
    ego_obs_t = ego_obs;
%     R = get_reward(ego_obs, Cars(1), collision_flag, Params);
    
    % update Cars
    [Cars, dist, ego_action,ho,dr,ego_v] = Q_update_dynamics(Cars, dist, Params, Q, epsilon);
    
    % statistics
%    N(s, ego_action) = N(s, ego_action) + 1;
    HO_total = HO_total +ho;
    HO_density = HO_total/dist;
    
    %initial an velocity_array
    
    v_array_q(time) = Cars(1).vx;
    ho_array_q(time) = ho;
    ho_density_array_q(time) = HO_density;
    
    
    % check if there is collision:
    collision_flag = check_collision(Cars, Params);
    
    % update ego_obs
    % ego_obs of next time step:
    ego_obs = get_Observation(1, Cars, Params);
    [R_transportation,R_telecome,R] = get_reward(ego_obs_t, Cars(1), collision_flag, Params,HO_density);
    
    a = ego_action;
    % update state to next state
    s_n = obs2state(ego_obs, Params);
    
   %% 
    if collision_flag == false
        
        Q(s,a) = Q(s,a) + alpha * (R + gamma * max(Q(s_n,:)) - Q(s,a));
        
        if plot_flag
            Plot_Traffics_not_saving_gif(Cars, time, dist, Params, ego_obs);
        end
    else
        % filter bad initialization
        if time >= 3
            
            Q(s,a) = Q(s,a) + alpha * (R + gamma * max(Q(s_n,:)) - Q(s,a));
            score = (dist-4000)/1000;
            ho_density = HO_total/dist ; % instant velocity  * dist/time
           
            
            DR_total = DR_total + dr;
            DR_average = DR_total / time;
            Average_Speed  = dist/time;
            disp('Collision!');
            fprintf('Distance Travelled = %3.4d m\n', dist);
            fprintf('Average Speed      = %3.2f m/s\n', dist/time);
            fprintf('Score              = %3.2f \n', score);
            fprintf('Average dr              = %8.2f \n', DR_average);
            fprintf('HO_density              = %3.2f \n', HO_density);
            fprintf('HO             = %3.2f \n', HO_density);
            if plot_flag
                Plot_Traffics_not_saving_gif(Cars, time, dist, Params, ego_obs);
            end
        else
            disp('Bad initialization!');
        end
        
        break; % break the current episode & restart simulation
        
    end % collision check
    
end % for time

%%
if collision_flag == false
    score = (dist-4000)/1000;
    disp('No Collision');
    fprintf('Distance Travelled = %3.4d m\n', dist);
    fprintf('Average Speed      = %3.2f m/s\n', dist/time);
    fprintf('Score              = %3.2f \n', score);
%     fprintf('Average dr              = %8.2f \n', DR_average);
%     fprintf('HO_density              = %3.2f \n', HO_density);
%     fprintf('HO             = %3.2f \n', HO_density);
end

return



