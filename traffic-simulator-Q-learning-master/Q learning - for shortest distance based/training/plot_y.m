function plot_dr(dr, episode, collision_flag, epsilon,plot_y)
figure(3);
if dr ~= 0
    if collision_flag == true
        plot(episode, dr, '.r');
    else
        plot(episode, dr, '.b');
    end
end
hold on;

if episode == 1
    xlabel('episode');
    ylabel(plot_y);
end

legend(['eps = ' num2str(epsilon)]);
drawnow;

end
