
function train_Q(Q, setting_str, episode_limit, time_limit, plot_flag,...
                    epsilon, if_eps_decay, decay_rate, gamma, alpha)

max_states_num = 3^7 * 2;%3^6 * 2
action_num = 7;

N = zeros(max_states_num, action_num);
score = zeros(episode_limit, 1);
ho_density = zeros(episode_limit, 1);
DR_average = zeros(episode_limit, 1);
R_transportation = zeros(episode_limit, 1);
R_telecome = zeros(episode_limit, 1);
R = zeros(episode_limit, 1);
Average_Speed = zeros(episode_limit, 1);
v_array = [];
ho_array = [];
ho_density_array = [];
% v_array = zeros(episode_limit,Params.horizon);

K = decay_rate / episode_limit;


start_time = tic;
for episode = 1 : episode_limit
    
    % epsilon anealing
    if if_eps_decay
        epsilon = 1 * exp(- K * episode);
    end
    
    fprintf('\nepisode = %d\n', episode);
    num_env_cars = randi(5) + 20;

    [Q, score(episode),ho_density(episode),DR_average(episode),R_transportation(episode),R_telecome(episode),R(episode),Average_Speed(episode),v_array_q,ho_array_q,ho_density_array_q, collision_flag, N] ...
        = Q_learning(Q, N, num_env_cars, plot_flag, epsilon, gamma, alpha);
    
     v_array = [v_array; v_array_q];
     ho_array = [ho_array; ho_array_q];
     ho_density_array =[ho_density_array; ho_density_array_q];
    

    
    plot_y(score(episode), episode, collision_flag, epsilon,'score'); 
    plot_y(R_telecome(episode), episode, collision_flag, epsilon,'R_telecome');
    plot_y(R_transportation(episode), episode, collision_flag, epsilon,'R_transport');
    plot_y(R(episode), episode, collision_flag, epsilon,'Total R_transport');
    
    
%     plot_dr(DR_average(episode), episode, collision_flag, epsilon);
    
    if mod(episode, 500) == 0
        s1 = int2str(episode);
        s2 = 'episode_';
        s = strcat(s2,s1);
        
        set_str = strcat(s,setting_str);
        
        cd Parameters
        csvwrite(['Q_' set_str '.csv'], Q);
        csvwrite(['score_' set_str '.csv'], score);
        csvwrite(['N_' set_str '.csv'], N);
        csvwrite(['ho_density_' set_str '.csv'], ho_density);
        csvwrite(['DR_' set_str '.csv'], DR_average);
        csvwrite(['R_transportation_' set_str '.csv'], R_transportation);
        csvwrite(['R_telecome_' set_str '.csv'], R_telecome);
        csvwrite(['Reward_' set_str '.csv'], R);
        csvwrite(['average_speed_' set_str '.csv'], Average_Speed);
        [~, Policy_1] = max(Q,[],2);
        csvwrite(['Policy_1_' set_str '.csv'], Policy_1);
        cd ..
%         plot_rewards(R_transportation,R_telecome,R,set_str);

    cd v_array
        setting_str1 = setting_str;
        s1 = int2str(episode);
        s2 = 'episode_';
        s3 = datestr(now,'HH_MM_SS');
        s = strcat(s2,s1);
        s = strcat(s3,s);
        set_str = strcat(s,setting_str1);
        csvwrite(['v_array_' set_str '.csv'], v_array);
        csvwrite(['ho_array_' set_str '.csv'], ho_array);
        csvwrite(['ho_density_array_' set_str '.csv'], ho_density_array);
    cd ..

    end
    
    if toc(start_time) > time_limit
        break;
    end
    
    if plot_flag == true
        close all;
    end
    
end % for episode

cd Parameters
csvwrite(['Q_' setting_str '.csv'], Q);
csvwrite(['score_' setting_str '.csv'], score);
csvwrite(['N_' setting_str '.csv'], N);
csvwrite(['ho_density_' setting_str '.csv'], ho_density);
csvwrite(['DR_' setting_str '.csv'], DR_average);
csvwrite(['R_transportation_' setting_str '.csv'], R_transportation);
csvwrite(['R_telecome_' setting_str '.csv'], R_telecome);
csvwrite(['Reward_' setting_str '.csv'], R);
csvwrite(['average_speed_' setting_str '.csv'], Average_Speed);
[~, Policy_1] = max(Q,[],2);
csvwrite(['Policy_1_' setting_str '.csv'], Policy_1);
cd ..

cd v_array
    setting_str1 = setting_str;
    s1 = int2str(episode);
    s2 = 'episode_';
    s3 = datestr(now,'HH_MM_SS');
    s = strcat(s2,s1);
    s = strcat(s3,s);
    set_str = strcat(s,setting_str1);
    csvwrite(['v_array_' set_str '.csv'], v_array);
    csvwrite(['ho_array_' set_str '.csv'], ho_array);
    csvwrite(['ho_density_array_' set_str '.csv'], ho_density_array);
cd ..
figure;
image(N);
