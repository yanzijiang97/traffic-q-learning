function [dr,bs] = get_sinr(Cars,index)%angle_list

%% =================== RF Parameters ======================================
alpha =2.5;                       %Path Loss Exponent better to have indoor small
PR = 1;
fcRF=2.1*10^9;
GT=1;
GR=1;
gammaI=(3*10^8)^2*GT*GR/(16*pi^2*fcRF^2); %used in sinr 
 
%% ===================== THz Parameters ===========================
PT = 1;                             % Tranmitted Power
kf = 0.05;                           % Absorbtion loss
fcTH=1.0*10^12;
GTT=316.2;
GRR=316.2;
% GTT=31.62;
% GRR=31.62;

thetabs=pi/6;%%%in degrees
thetamt=pi/6;%%%in degrees
FBS=thetabs/(2*pi);
FMT=thetamt/(2*pi);
prob= FBS*FMT;
gammaII=(3*10^8)^2*GTT*GRR/(16*pi^2*fcTH^2);
 

R_max = Params.nominal_speed *35;%100
Nit = 10000;
%lambdas=10;
%% ===================== Rate and SINR theshold calculation ========================================
Rate = [5 ]*10^9;
Wt=5*10^8;
Wr=40*10^6;
% SINRthRF =2.^(Rate./(Wr))-1; %thresholds
% SINRthTH =2.^(Rate./(Wt))-1;

Bias=[1000 100 1 0.001 0.0001 0.0001 0.00001];%%%0.05
%Bias=[ 10^6  10^5 10^4  10^4 10^3 10^3 10^3];%%%0.2

%if type == 'RF'
    
%    sinr = 1
   
      for i=1:200%Nit
        
        UoI = 1;
        lambda=2;
        % The index of User of Interest
        NRF = 5;
        NTHzT = 5;
       % Nu = 30;
       Nu = length(Cars);
  
        
        fadeRand = exprnd(1,NRF,UoI);
        
      
            rue = R_max*sqrt(rand(Nu,1));
            thetau = 2*pi*rand(Nu,1);
%             Xu = rue.*cos(thetau);
%             Yu = rue.*sin(thetau);
%         array_x = [Cars.x];

%             Xu = Cars(1).x.*cos(thetau);
%             Yu = Cars(1).y.*sin(thetau);
%             array_x = [Cars.x].';
            Xu = [Cars.x].'.*cos(thetau);
            Yu = [Cars.y].'.*sin(thetau);
            
            
            rTHzbs = R_max*sqrt(rand(NTHzT,1));
            thetaTb = 2*pi*rand(NTHzT,1);
            Xb = rTHzbs.*cos(thetaTb);
            Yb = rTHzbs.*sin(thetaTb);
            
            rRFzbs = R_max*sqrt(rand(NRF,1));
            thetaRb = 2*pi*rand(NRF,1);
            Xrb = rRFzbs.*cos(thetaRb);
            Yrb = rRFzbs.*sin(thetaRb);
            

            
            
            % Distacnces from THz BSs
            
            [Xmp_Tmat, Xp_Tmat] = meshgrid(Xu,Xb);
            [Ymp_Tmat, Yp_Tmat] = meshgrid(Yu,Yb);
            D_ue_Tbs = sqrt((Xmp_Tmat-Xp_Tmat).^2 + (Ymp_Tmat-Yp_Tmat).^2);
            
            % Distacnces from RF BSs
            
            [Xmp_Rmat, Xp_Rmat] = meshgrid(Xu,Xrb);
            [Ymp_Rmat, Yp_Rmat] = meshgrid(Yu,Yrb);
            D_ue_Rbs = sqrt((Xmp_Rmat-Xp_Rmat).^2 + (Ymp_Rmat-Yp_Rmat).^2);
            
            fadeRand = exprnd(1,NRF,Nu);
            SRF=gammaI.*fadeRand.*PR.*D_ue_Rbs.^(-alpha); %signal matrix for RF
            NP=(10)^-10;
            interf=repmat(sum(SRF,1),NRF,1)-SRF; %interference for RF
            RPrAllu1 = Wr * log2(1+SRF./(NP+interf)); %power from all base-stations to all users
%             RPrAllu = log2(1+SRF./NP);

            
            
            fadeRand1 = exprnd(1,NTHzT,Nu);
            STHz=gammaII.*fadeRand1.*PT.*exp(-kf.*D_ue_Tbs)./(D_ue_Tbs.^2); %signal matrix for THZ
            interfT=repmat(sum(STHz,1),NTHzT,1)-STHz; %interference matrix for THz
            TPrAllu1 = Wt * log2(1+STHz./(NP+interfT));
            
            
            %  return 3 maximum sinr in RF BS
            % because we want to find first column only for the ego car
           ego_car_RPr = RPrAllu1(:,index).';
           [RF_max, RF_index] = maxk(ego_car_RPr, 3, 2);
           RF_ego = RF_max(1,:);
           RF_index = RF_index(1,:);
           RRF_index =  RF_index; % integer array to get the location
           RF_index =  'r' + string(RF_index);
           rbs_x = Xb(RRF_index);
           rbs_y = Yb(RRF_index);
           
%            [RF_max, RF_index] = maxk(RPrAllu1, 3, 2);% 1
%            rf_bs_index = RF_index;
%            RF_ego = RF_max(1,:);
%            RF_index = RF_index(1,:);
%            RF_index =  'r' + string(RF_index);

%            B = maxk(RF_max,3,2)
%            [~,RF] = maxk(RPrAllu1,3); % find top 3 index
%            RF = RF+'r';
           
            %  return 3 maximum sinr in THz BS
            ego_car_TPr = TPrAllu1(:,index).';
           [THz_max, THz_index] = maxk(ego_car_TPr, 3, 2);
            Thz_ego = THz_max(1,:);
            THz_index = THz_index(1,:);
            TTHz_index =  THz_index; % integer array to get the location
            THz_index =  't' + string(THz_index);
           tbs_x = Xb(TTHz_index);
           tbs_y = Yb(TTHz_index);
            
            
           
            dis_ego_rf = D_ue_Rbs(:,1);
            [dis_ego_rf,Index_rf] = max(dis_ego_rf);
            
            dis_ego_thz = D_ue_Tbs(:,1);
            dr = 0;
            bs = '';
            [dis_ego_thz,Index_thz] = max(dis_ego_thz);
            if(dis_ego_rf < dis_ego_thz)
                dr = TPrAllu1(Index_thz,1);
                bs =  't' + string(Index_thz);
            else
                dr = RPrAllu1(Index_rf,1);
                bs = 'r' + string(Index_rf);
            end
  

end

