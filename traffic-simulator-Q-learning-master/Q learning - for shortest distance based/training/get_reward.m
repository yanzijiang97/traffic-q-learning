function [R_transportation,R_telecome,R] = get_reward(obs, ego_car, colli, Params,HO_density)
msg = obs2msg(obs, Params);
% input: message of the ego car; struct of the ego
% car; a flag of whether the ego car caused a collision --boolean; and Global Params

% output: return a immediate reward r(s,a)

% NYX modified 12/03/2017 16:08;
w1 = 1000; %collision default 1000 100
w2 = 5; %velocity
w3 = 1; %headway
w4 = 1; %effort
w5 = 1; %lane --> should be tuned later on
w6 = 4.5/10^6;

c = - colli; %{-1 if colli, 0 else}
vn = (ego_car.vx - Params.nominal_speed)/5; %scaled velocity \in [-1, 1]
h = msg.fc_d - 2; %{-1 if near, 0 if mid, 1 if far}
l = -(ego_car.lane_id - 1); % {-1 if left, 0 if right}

% a = 0 if maintain; a = -5 if hard_accel or hard_decel; a = -1 if else;
a = -1;
if ego_car.a == 0 && ego_car.vy == 0
    a = 0;
elseif ego_car.a >= Params.accel_hard || ego_car.a <= Params.decel_hard
    a = -5;
end


%% telecommunication side
% dr_ego = max([ego_car.bs1,ego_car.bs2,ego_car.bs3]);% this is not the maximum, is currently connected

dr_ego = ego_car.bs1;
R_transportation =  w1*c + w2*vn + w3*h + w4*a + w5*l;
% R_telecome =  w6 * dr_ego+ w7 * HO_density*ego_car.vx;

R_telecome = w6 * dr_ego * (1- min(1,HO_density*ego_car.vx));
 



%%average data_rate of all cars

% w1*c + w2*vn + w3*h + w4*a + w5*l + w6 * dr_ego+ w7 * HO_density
R = R_transportation +R_telecome ; %;+ w7 * HO_total;+ w6 * dr_ego+ w7 * HO_density*ego_car.vx

