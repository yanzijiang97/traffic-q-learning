function plot_rewards(R_transportation,R_telecome,R,set_str)

%initializing
Z = zeros(length(R));
Z_tele = zeros(length(R));
Z_trans = zeros(length(R));
%specify class width
class = 50;

for r = 1:length(R)-class
   Z(r) = mean(R(r:r+class));
   Z_tele(r) = mean(R_telecome(r:r+class));
   Z_trans(r) = mean(R_transportation(r:r+class));
   
   
end

plot(Z);
title('Total rewards')
xlabel('Episodes')
ylabel('Total rewards')
cd plot
savefig(['total_reward_plot_' set_str '.fig'])
cd ..



plot(Z_tele);
% plot(Z_tele);
title('Telecome rewards')
xlabel('Episodes')
ylabel('tele rewards')
cd plot
savefig(['tele_reward_plot_' set_str '.fig'])
cd ..



plot(Z_trans);
title('Transportation rewards')
xlabel('Episodes')
ylabel('transportation rewards')
cd plot
savefig(['trans_reward_plot_' set_str '.fig'])
cd ..

end