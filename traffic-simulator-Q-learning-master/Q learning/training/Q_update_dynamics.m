% lisheng 12/4/17

function [Cars, dist, ego_action, ho, dr_ego, ego_v] = Q_update_dynamics(Cars, dist, Params, Q, epsilon,Xb,Yb,Xrb,Yrb,NTHzT,NRF)
dt = Params.time_step;
action_num = 9; %6
ho = 0;
acc = 0;%acceleration
%ego_v = -1; %default -1

% get current observation of each car
for car_id = 1:length(Cars)
    obs_of_all_cars(car_id) = get_Observation(car_id, Cars, Params);
    %     Cars(car_id).max_bs = obs_of_all_cars(car_id).best_bs;
    %% telecommunication side
 
    %     best_bs = bs(3);
 
end

% update position: x and y
for car_id = 1:length(Cars)
%     if car_id ~= 1 % env car action selection
%         action_id = policy_0(obs_of_all_cars(car_id), Params);
%      
%         if action_id < 7 % ~= action_id == 1 is for 0 0 0 case
%             dynamics = act2dyn(action_id, Params);
%             acc = dynamics(1);
%             Cars(car_id).vy = dynamics(2);
%          
%         elseif action_id == 7 % collision
%             acc = - Cars(car_id). vx / dt * 0.85;
%             Cars(car_id).vy = 0;
%          
%         end
     
%     else % ego car action selection ,Xrb,Yrb,NRF,NTHzT
        ego_state = obs2state(obs_of_all_cars(car_id), Params);

%     if car_id ~= 1 % env car action selection
%         ego_action = policy_0(obs_of_all_cars(car_id), Params);
%     
%     else

        if rand() <= epsilon
            rand1 = randi(action_num);
            while (rand1 == 7)
                rand1 = randi(action_num); %avoid random number to stop state
            end
         
            ego_action = rand1; % Return ego_action = 1-9 except 7
     else
            [~, ego_action] = max(Q(ego_state, :));
        end
     
%     end
        [dr, bs] = get_sinr(Cars, car_id,Xb,Yb,Xrb,Yrb,NRF,NTHzT);

        Cars(car_id).bs1 = dr(1);
        Cars(car_id).bs2 = dr(2);
        Cars(car_id).bs3 = dr(3);
     
        Cars(car_id).max_bs = bs(1);
     
     
%         if rand() <= epsilon
%             rand1 = randi(action_num);
%          
% %             while (rand1 == 7)
% %                 rand1 = randi(action_num); %avoid random number to stop state
% %             end
%          
%             ego_action = rand1; % Return ego_action = 1-9 except 7
%         else
%             [~, ego_action] = max(Q(ego_state, :));
%          
%         end
     
        dr_ego = max([Cars(car_id).bs1, Cars(car_id).bs2, Cars(car_id).bs3]);
     
        if ego_action < 7 % ~=
            dynamics = act2dyn(ego_action, Params);
            acc = dynamics(1);
            Cars(car_id).vy = dynamics(2);


        elseif ego_action == 7 % collision
            acc = - Cars(car_id). vx / dt * 0.85;
            Cars(car_id).vy = 0;
         
        elseif ego_action == 8 % 1 0 0 % 1 1 0 % 1 1 1 
            bs_candidate = [bs(1), bs(2),bs(3)]; % base station id
            dr_candidate = [dr(1), dr(2),dr(3)]; % data rate

            % number_of_connected(3,9,8)
            % (5,2,5)

            %coefficent(1/3,1/2,1/5)

            coefficient = ones(1, length(bs_candidate)) ./ min([1+number_of_connected(bs_candidate, Cars), quota(bs_candidate)]); % /number_of_connected(bs_candidate, Cars);%
%             coefficient = 1;
            dr_withquota = dr_candidate .* coefficient;

            dr_max_quota = max(dr_withquota);
            
             if dr_max_quota == dr_withquota(1)
                Cars(car_id).max_bs = bs(1);
                Cars(car_id).bs1 = dr_withquota(1);

            elseif dr_max_quota == dr_withquota(2)
                Cars(car_id).max_bs = bs(2);
                Cars(car_id).bs1 = dr_withquota(2);
             
            elseif dr_max_quota == dr_withquota(3)
                Cars(car_id).max_bs = bs(3);
                Cars(car_id).bs1 = dr_withquota(3);
             
            end
         

        elseif ego_action == 9

                Cars(car_id).max_bs = '';
                Cars(car_id).bs1 = 0;
                Cars(car_id).bs2 = 0;
                Cars(car_id).bs3 = 0;


        end
                % handovers calculation
        if (car_id == 1) && (strcmpi(Cars(car_id).max_bs, Cars(car_id).prev_bs) == false)
            ho = ho + 1;
            Cars(car_id).prev_bs = Cars(car_id).max_bs; % update the base station
            %disp("hand off plus")
        end
        
%     end
 
    % update velocity using dynamics
    Cars(car_id).a = acc;
    Cars(car_id).vx = Cars(car_id).vx + acc * dt;
 
    % speed limit
    if Cars(car_id).vx > Params.max_speed
        Cars(car_id).a = 0; % Cars(car_id).a = (Cars(car_id).vx - Params.max_speed)/dt;
        Cars(car_id).vx = Params.max_speed;
    elseif Cars(car_id).vx < Params.min_speed
        Cars(car_id).a = 0;
        Cars(car_id).vx = Params.min_speed;
    end
 
    % update position using dynamics
    % x
    Cars(car_id).x = Cars(car_id).x + Cars(car_id).vx * dt;
    % y, update y and lane_id if vy ~= 0
    if Cars(car_id).vy ~= 0
        if Cars(car_id).lane_id == 1
            Cars(car_id).y = Cars(car_id).y + Cars(car_id).vy * dt;
            Cars(car_id).lane_id = 2;
        else
            Cars(car_id).y = Cars(car_id).y - Cars(car_id).vy * dt;
            Cars(car_id).lane_id = 1;
        end
    end
 
    % set relative x - coordinate (wrt ego car)
    Cars(car_id).x = Cars(car_id).x - Cars(1).vx * dt;
    Cars(car_id).theta = Cars(car_id).x / Params.road_radius;
 
    % re-regulate to -pi~pi
    if Cars(car_id).theta > pi
        Cars(car_id).theta = Cars(car_id).theta - 2 * pi;
    elseif Cars(car_id).theta < - pi
        Cars(car_id).theta = Cars(car_id).theta + 2 * pi;
    end
 
    Cars(car_id).x = Cars(car_id).theta * Params.road_radius;
 
    % calculate the distance ego car has travelled
    %     ego_v = -1;
    if car_id == 1
        dist = dist + Cars(car_id).vx * dt;
        ego_v = Cars(car_id).vx;
    end
    Cars(car_id).x = Cars(car_id).x +dist;
 
end % update position: x and y

return

function result = number_of_connected(bs_candidate, Cars)

result = zeros(1,length(bs_candidate));
ind = 1;
for member = bs_candidate
    s = string(member);
    str = string([Cars.('max_bs')]);
    bs_count = sum(count(str,s));
    result(ind) = bs_count;
    ind = ind + 1;
end

end

function result = quota(bs_candidate)

result = zeros(1, length(bs_candidate));

ind = 1;
for member = bs_candidate
    %      disp(['the bs is ' member])
    s = string(member);
    if (s{1}(1) == 'r')
        result(ind) = 2;
    else %string(member).get(0) == 't'
        result(ind) = 5;
    end
    ind = ind + 1;
end
end



end
