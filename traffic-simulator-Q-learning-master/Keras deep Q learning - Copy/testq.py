
import pandas as pd
import matplotlib.pyplot as plt


path = r'E:\research\1120\traffic-simulator-Q-learning-master\Parameters\score_alpha_0.2_episode_5000_w2_10_w5_0.1_epsilon_0.1.csv'
df = pd.read_csv(path,header=None)  
df = df.replace(0, -4)
EPISODES, col = df.shape
print(df.shape)
print(df)



bin_num = 200
x =[]
y= []
i= 'score'
for j in range(int(EPISODES/bin_num)):
        x.append(j * bin_num)
        y.append(df[j * bin_num:j * bin_num+(bin_num-1)].mean())
plt.title(i)

# plt.legend(i + "_mean")
plt.plot(x, y , label = "v=20")
plt.grid()
file = path[:-20]
print(file)
plt.savefig(path + str(bin_num) +'.png')
plt.show()
